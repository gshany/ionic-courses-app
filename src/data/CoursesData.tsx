export let COURSES_DATA = [
  {
    id: "c1",
    title: "The ionic + react clush course",
    enrollmentDate: new Date("03/05/2020"),
    goals: [
      {
        id: "c1g1",
        text: "Become smarter"
      },
      {
        id: "c1g2",
        text: "Gain more money"
      },
      {
        id: "c1g3",
        text: "Be the best"
      }
    ]
  },
  {
    id: "c2",
    title: "Gitlab - from start to bottom",
    enrollmentDate: new Date("12/07/2018"),
    goals: [
      {
        id: "c2g1",
        text: "Become smarter"
      },
      {
        id: "c2g2",
        text: "Gain more money"
      }
    ]
  },
  {
    id: "c3",
    title: "React from scratch course",
    enrollmentDate: new Date("03/09/2019"),
    goals: [
      {
        id: "c3g1",
        text: "Become smarter"
      },
      {
        id: "c3g2",
        text: "Gain more money"
      }
    ]
  }
];
