import React, { useContext, useRef, useState } from "react";
import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonModal,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { editedGoalObject } from "../pages/CourseGoals";

interface EditModalProps {
  editedGoal: editedGoalObject;
  shouldOpenModal: boolean;
  onCancel: () => void;
  onSave: (text: string) => void;
}

const EditModal: React.FC<EditModalProps> = (props) => {
  const goalInputRef = useRef<HTMLIonInputElement>(null);
  const [errorMsg, setErrorMsg] = useState<string>("");

  const handleSaveGoal = () => {
    const goalText = goalInputRef.current!.value;

    if (!goalText || goalText.toString().trim().length === 0) {
      setErrorMsg("please provide valid input values");
      return;
    }

    props.onSave(goalText.toString());
    setErrorMsg("");
  };

  const { editedGoal } = props;
  return (
    <IonModal isOpen={props.shouldOpenModal}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>{editedGoal ? "Edit" : "Add"} Goal</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <IonItem>
                <IonLabel position="floating">Your goal</IonLabel>
                <IonInput ref={goalInputRef} value={editedGoal?.text} type="text" />
              </IonItem>
            </IonCol>
          </IonRow>
          {errorMsg && (
            <IonRow className="ion-text-center">
              <IonCol>
                <IonText color="danger">
                  <p>{errorMsg}</p>
                </IonText>
              </IonCol>
            </IonRow>
          )}
          <IonRow className="ion-text-center">
            <IonCol>
              <IonButton onClick={props.onCancel} fill="clear">
                Cancel
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton expand="block" onClick={handleSaveGoal}>
                Save
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonModal>
  );
};

export default EditModal;
