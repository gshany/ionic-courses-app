import {
  IonButton,
  IonCol,
  IonContent,
  IonDatetime,
  IonGrid,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonModal,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { text } from "ionicons/icons";
import React, { useRef, useState } from "react";

interface AddCourseModalProps {
  shouldOpenModal: boolean;
  onCancel: () => void;
  onSave: (text: string, date: Date) => void;
}

const AddCourseModal: React.FC<AddCourseModalProps> = (props) => {
  const textInputRef = useRef<HTMLIonInputElement>(null);
  const dateInputRef = useRef<HTMLIonDatetimeElement>(null);
  const [errorMsg, setErrorMsg] = useState<string>("");

  const handleSaveNewCourse = () => {
    const textValue = textInputRef.current!.value;
    const dateValue = dateInputRef.current!.value;

    if (
      !textValue ||
      !dateValue ||
      textValue.toString().trim().length === 0 ||
      dateValue.toString().trim().length === 0
    ) {
      setErrorMsg("please enter valid inputs and try again");
      return;
    }

    setErrorMsg("");

    props.onSave(textValue.toString(), new Date(dateValue));
  };

  return (
    <IonModal isOpen={props.shouldOpenModal}>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Add New Course</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <IonItem>
                <IonLabel position="floating">Course Name</IonLabel>
                <IonInput ref={textInputRef} type="text" />
              </IonItem>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonItem>
                <IonLabel>Enrollment Date</IonLabel>
                <IonDatetime ref={dateInputRef} displayFormat="DD MM, YYYY" />
              </IonItem>
            </IonCol>
          </IonRow>
          {errorMsg && (
            <IonRow className="ion-text-center">
              <IonCol>
                <IonText color="danger">
                  <p>{errorMsg}</p>
                </IonText>
              </IonCol>
            </IonRow>
          )}
          <IonRow className="ion-text-center">
            <IonCol>
              <IonButton fill="clear" onClick={props.onCancel}>
                Cancel
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton expand="block" onClick={handleSaveNewCourse}>
                Save
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonModal>
  );
};

export default AddCourseModal;
