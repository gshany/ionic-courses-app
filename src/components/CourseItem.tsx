import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardSubtitle,
  IonCardTitle
} from "@ionic/react";
import React from "react";

interface CourseItem {
  id: string;
  title: string;
  enrollmentDate: Date;
}

const CourseItem: React.FC<CourseItem> = (props) => {
  return (
    <IonCard>
      <IonCardContent>
        <IonCardTitle>{props.title}</IonCardTitle>
        <IonCardSubtitle>
          Enrolled on{" "}
          {props.enrollmentDate.toLocaleDateString("en-US", {
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
          })}
        </IonCardSubtitle>
        <div className="ion-text-right">
          <IonButton
            className="ion-margin-top"
            routerLink={`/courses/${props.id}`}
            color="primary"
            fill="clear"
          >
            View course data
          </IonButton>
        </div>
      </IonCardContent>
    </IonCard>
  );
};

export default CourseItem;
