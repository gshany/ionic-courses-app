import {
  IonIcon,
  IonItem,
  IonItemOption,
  IonItemOptions,
  IonItemSliding,
  IonLabel
} from "@ionic/react";
import { createOutline, trashOutline } from "ionicons/icons";
import React, { RefObject } from "react";

interface EditableCourseGoalProps {
  text: string;
  onStartDelete: () => void;
  onStartEdit: (event: React.MouseEvent) => void;
  optionsRef: RefObject<HTMLIonItemSlidingElement>;
}

const EditableCourseGoal: React.FC<EditableCourseGoalProps> = (props) => {
  return (
    <IonItemSliding ref={props.optionsRef}>
      <IonItemOptions side="start">
        <IonItemOption color="none" onClick={props.onStartDelete}>
          <IonIcon color="danger" slot="icon-only" icon={trashOutline}></IonIcon>
        </IonItemOption>
      </IonItemOptions>
      <IonItem lines="full">
        <IonLabel>{props.text}</IonLabel>
      </IonItem>
      <IonItemOptions side="end">
        <IonItemOption color="none" onClick={props.onStartEdit}>
          <IonIcon color="primary" slot="icon-only" icon={createOutline}></IonIcon>
        </IonItemOption>
      </IonItemOptions>
    </IonItemSliding>
  );
};

export default EditableCourseGoal;
