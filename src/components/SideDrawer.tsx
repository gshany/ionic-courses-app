import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { list, options } from "ionicons/icons";
import React from "react";

const SideDrawer: React.FC = () => {
  return (
    <IonMenu contentId="main">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Filters</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonMenuToggle>
            <IonItem button routerLink="/filters" routerDirection="none">
              <IonLabel>filters</IonLabel>
              <IonIcon icon={list} slot="start"></IonIcon>
            </IonItem>
          </IonMenuToggle>
          <IonMenuToggle>
            <IonItem button routerLink="/courses/all-goals" routerDirection="none">
              <IonLabel>all goals</IonLabel>
              <IonIcon icon={options} slot="start"></IonIcon>
            </IonItem>
          </IonMenuToggle>
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default SideDrawer;
