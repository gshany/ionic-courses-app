import React from "react";

export interface Goal {
  id: string;
  text: string;
}

export interface Course {
  id: string;
  title: string;
  enrollmentDate: Date;
  goals: Goal[];
}

interface CoursesContextInterface {
  courses: Course[];
  activeCourses: string[];
  addNewCourse: (courseTitle: string, enrollmentDate: Date) => void;
  addNewGoal: (courseId: string, text: string) => void;
  deleteGoal: (courseId: string, goalId: string) => void;
  updateGoal: (courseId: string, goalId: string, newText: string) => void;
  addCourseFilter: (courseId: string) => void;
  removeCourseFilter: (courseId: string) => void;
}

const CoursesContext = React.createContext<CoursesContextInterface>({
  courses: [],
  activeCourses: [],
  addNewCourse: () => {},
  addNewGoal: () => {},
  deleteGoal: () => {},
  updateGoal: () => {},
  addCourseFilter: () => {},
  removeCourseFilter: () => {}
});

export default CoursesContext;
