import React, { useState } from "react";
import CoursesContext, { Course, Goal } from "./CoursesContext";

import { COURSES_DATA } from "../data/CoursesData";

const CoursesContextProvider: React.FC = (props) => {
  const [courses, setCourses] = useState<Course[]>(COURSES_DATA);
  const [activeCourses, setActiveCourse] = useState<string[]>([]);

  const addNewCourse = (courseTitle: string, enrollmentDate: Date) => {
    const newCourse: Course = {
      id: Math.random.toString(),
      title: courseTitle,
      enrollmentDate,
      goals: []
    };

    setCourses((currCourses) => {
      return currCourses.concat(newCourse);
    });
  };

  const addNewGoal = (courseId: string, text: string) => {
    const newGoal: Goal = {
      id: Math.random().toString(),
      text
    };

    setCourses((currCourses) => {
      return currCourses.map((course) => {
        return course.id !== courseId
          ? course
          : { ...course, goals: course.goals.concat(newGoal) };
      });
    });
  };

  const deleteGoal = (courseId: string, goalId: string) => {
    setCourses((currCourses) => {
      return currCourses.map((course) => {
        return course.id !== courseId
          ? course
          : { ...course, goals: course.goals.filter((goal) => goal.id != goalId) };
      });
    });
  };

  const updateGoal = (courseId: string, goalId: string, newText: string) => {
    setCourses((currCourses) => {
      return currCourses.map((course) => {
        return course.id !== courseId
          ? course
          : {
              ...course,
              goals: course.goals.map((goal) => {
                return goal.id !== goalId ? goal : { ...goal, text: newText };
              })
            };
      });
    });
  };

  const addCourseFilter = (courseId: string) => {
    setActiveCourse((currFilters) => {
      return currFilters.concat(courseId);
    });
  };

  const removeCourseFilter = (courseId: string) => {
    setActiveCourse((currFilters) => {
      return currFilters.filter((courseFilter) => courseFilter === courseId);
    });
  };

  return (
    <CoursesContext.Provider
      value={{
        courses,
        activeCourses,
        addNewCourse,
        addNewGoal,
        deleteGoal,
        updateGoal,
        addCourseFilter,
        removeCourseFilter
      }}
    >
      {props.children}
    </CoursesContext.Provider>
  );
};

export default CoursesContextProvider;
