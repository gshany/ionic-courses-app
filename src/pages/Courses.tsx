import React, { useState, useContext } from "react";
import {
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonGrid,
  IonHeader,
  IonIcon,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
  isPlatform
} from "@ionic/react";
import { add } from "ionicons/icons";

import AddCourseModal from "../components/AddCourseModal";
import CourseItem from "../components/CourseItem";
import CoursesContext from "../react-context/CoursesContext";

interface EditedCourse {
  id: string;
  title: string;
  enrollmentDate?: Date;
}

export type EditedCourseObject = EditedCourse | null;

const Courses: React.FC = () => {
  const [isAddingNewCourse, setIsAddingNewCourse] = useState<boolean>(false);
  const coursesCtx = useContext(CoursesContext);

  const handleAddingNewCourse = () => {
    // prompts the 'Add New Course' modal
    setIsAddingNewCourse(true);
  };

  const handleAddingNewCourseCancelation = () => {
    setIsAddingNewCourse(false);
  };

  const handleSavingNewCourse = (courseTitle: string, enrollmentDate: Date) => {
    coursesCtx.addNewCourse(courseTitle, enrollmentDate);
    setIsAddingNewCourse(false);
  };

  return (
    <>
      <AddCourseModal
        shouldOpenModal={isAddingNewCourse}
        onCancel={handleAddingNewCourseCancelation}
        onSave={handleSavingNewCourse}
      />
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Courses</IonTitle>
            {!isPlatform("android") && (
              <IonButtons slot="end">
                <IonButton onClick={handleAddingNewCourse}>
                  <IonIcon class="icon-only" icon={add}></IonIcon>
                </IonButton>
              </IonButtons>
            )}
          </IonToolbar>
        </IonHeader>
        <IonContent>
          <IonGrid>
            {coursesCtx.courses.map((course) => (
              <IonRow key={course.id}>
                <IonCol size-md="4" offset-md="4">
                  <CourseItem
                    id={course.id}
                    title={course.title}
                    enrollmentDate={course.enrollmentDate}
                  />
                </IonCol>
              </IonRow>
            ))}
          </IonGrid>
          {isPlatform("android") && (
            <IonFab vertical="bottom" horizontal="end">
              <IonFabButton>
                <IonIcon icon={add} onClick={handleAddingNewCourse}></IonIcon>
              </IonFabButton>
            </IonFab>
          )}
        </IonContent>
      </IonPage>
    </>
  );
};

export default Courses;
