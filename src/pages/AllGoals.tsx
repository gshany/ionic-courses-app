import {
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import React, { useContext } from "react";
import CoursesContext from "../react-context/CoursesContext";

const AllGoals: React.FC = () => {
  const coursesCtx = useContext(CoursesContext);

  const filteredCoursesGoals = coursesCtx.courses
    .filter((course) =>
      coursesCtx.activeCourses.find((activeCourse) => activeCourse === course.id)
    )
    .map((course) => {
      return course.goals.map((goal) => {
        return { ...goal, courseId: course.id, courseTitle: course.title };
      });
    })
    .reduce((spreadedGoals, currentCourseGoals) => {
      let updatedArray = spreadedGoals;
      for (const goal of currentCourseGoals) {
        updatedArray = updatedArray.concat(goal);
      }

      return updatedArray;
    }, []);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton id="main" />
          </IonButtons>
          <IonTitle>All Goals</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          {filteredCoursesGoals.length > 0 ? (
            filteredCoursesGoals.map((courseGoal) => (
              <IonItem key={courseGoal.id}>
                <IonLabel className="ion-text-wrap">
                  <IonText color="primary">
                    <h2>{courseGoal.text}</h2>
                  </IonText>
                  <IonText>{courseGoal.courseTitle}</IonText>
                </IonLabel>
              </IonItem>
            ))
          ) : (
            <h2 className="ion-text-center">No Goals where found</h2>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default AllGoals;
