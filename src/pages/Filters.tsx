import React, { useContext } from "react";
import {
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToggle,
  IonToolbar
} from "@ionic/react";
import CoursesContext from "../react-context/CoursesContext";

const Filters: React.FC = () => {
  const coursesCtx = useContext(CoursesContext);

  const handleFilterToggleChange = (event: CustomEvent) => {
    const { checked, value } = event.detail;

    checked ? coursesCtx.addCourseFilter(value) : coursesCtx.removeCourseFilter(value);
    console.log(event);
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton id="main" />
          </IonButtons>
          <IonTitle>Filters</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          {coursesCtx.courses.map((course) => (
            <IonItem key={course.id}>
              <IonLabel>{course.title}</IonLabel>
              <IonToggle value={course.id} onIonChange={handleFilterToggleChange}></IonToggle>
            </IonItem>
          ))}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Filters;
