import React, { useContext, useRef, useState } from "react";
import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar,
  isPlatform,
  IonAlert,
  IonToast
} from "@ionic/react";
import { add } from "ionicons/icons";
import { useParams } from "react-router";

import EditModal from "../components/EditGoalModal";
import EditableCourseGoal from "../components/EditableCourseGoal";
import CoursesContext from "../react-context/CoursesContext";

interface editedGoal {
  id: string;
  text: string;
}

export type editedGoalObject = editedGoal | null;

const CourseGoals: React.FC = () => {
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = useState<boolean>(false);
  const [toastMessage, setToastMessage] = useState<string>("");
  const [isEditingGoal, setIsEditingGoal] = useState<boolean>(false);
  const [editedGoal, setEditedGoal] = useState<editedGoalObject>(null);

  const selectedCourseId = useParams<{ courseId: string }>().courseId;
  const slidingOptionsRef = useRef<HTMLIonItemSlidingElement>(null);
  let selectedGoalIdRef = useRef<string | null>(null);
  const coursesCtx = useContext(CoursesContext);

  const selectedCourse = coursesCtx.courses.find((course) => course.id === selectedCourseId);

  const deleteCourseGoalHandler = () => {
    setIsDeleteAlertOpen(false);
    setToastMessage("course goal deleted...");
    slidingOptionsRef.current?.closeOpened();
    coursesCtx.deleteGoal(selectedCourseId, selectedGoalIdRef.current!);
  };

  const startDeleteCourseGoalHandler = (goalId: string) => {
    setToastMessage("");
    setIsDeleteAlertOpen(true);
    selectedGoalIdRef.current = goalId;
  };

  const editCourseGoalHandler = (goalId: string, event: React.MouseEvent) => {
    event.stopPropagation();
    const editedGoal = selectedCourse?.goals.find((goal) => goalId === goal.id);
    if (!editedGoal) {
      return;
    }
    setIsEditingGoal(true);
    setEditedGoal(editedGoal);
    slidingOptionsRef.current?.closeOpened();
  };

  const stopEditCourseGoalHandler = () => {
    setIsEditingGoal(false);
    setEditedGoal(null);
  };

  const addCourseGoalHandler = () => {
    setIsEditingGoal(true);
  };

  const handleSaveGoal = (text: string) => {
    editedGoal
      ? coursesCtx.updateGoal(selectedCourse!.id, editedGoal.id, text)
      : coursesCtx.addNewGoal(selectedCourse!.id, text);
    setIsEditingGoal(false);
    setEditedGoal(null);
  };

  const content = !selectedCourse ? (
    <h2>No course was found</h2>
  ) : selectedCourse!.goals.length === 0 ? (
    <h2 className="ion-text-center">No goals were found</h2>
  ) : (
    <IonList>
      {selectedCourse!.goals.map((courseGoal) => (
        <EditableCourseGoal
          key={courseGoal.id}
          optionsRef={slidingOptionsRef}
          text={courseGoal.text}
          onStartDelete={startDeleteCourseGoalHandler.bind(null, courseGoal.id)}
          onStartEdit={editCourseGoalHandler.bind(null, courseGoal.id)}
        />
      ))}
    </IonList>
  );
  return (
    <>
      <EditModal
        shouldOpenModal={isEditingGoal}
        onCancel={stopEditCourseGoalHandler}
        onSave={handleSaveGoal}
        editedGoal={editedGoal}
      />
      <IonToast isOpen={!!toastMessage} message={toastMessage} duration={2000} />
      <IonAlert
        isOpen={isDeleteAlertOpen}
        header="Delete A Goal"
        message="Are you sure you want to delete the goal?"
        buttons={[
          {
            text: "No",
            role: "cancel",
            handler: () => {
              setIsDeleteAlertOpen(false);
            }
          },
          {
            text: "Yes",
            handler: deleteCourseGoalHandler
          }
        ]}
      />
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>
              {selectedCourse ? selectedCourse.title : "No course was found"}
            </IonTitle>
            <IonButtons slot="start">
              <IonBackButton defaultHref="/courses/list" />
            </IonButtons>
            {!isPlatform("android") && (
              <IonButtons slot="end">
                <IonButton onClick={addCourseGoalHandler}>
                  <IonIcon slot="icon-only" icon={add}></IonIcon>
                </IonButton>
              </IonButtons>
            )}
          </IonToolbar>
        </IonHeader>
        <IonContent>
          {content}
          {isPlatform("android") && (
            <IonFab vertical="bottom" horizontal="end" slot="fixed">
              <IonFabButton onClick={addCourseGoalHandler}>
                <IonIcon icon={add}></IonIcon>
              </IonFabButton>
            </IonFab>
          )}
        </IonContent>
      </IonPage>
    </>
  );
};

export default CourseGoals;
