import React from "react";
import {
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react";
import { list, trophyOutline } from "ionicons/icons";
import { Redirect, Route, Switch } from "react-router";
import AllGoals from "./AllGoals";
import CourseGoals from "./CourseGoals";
import Courses from "./Courses";

const CourseTabs: React.FC = () => {
  return (
    <IonTabs>
      <IonRouterOutlet id="main">
        <Switch>
          <Route path="/courses/list">
            <Courses />
          </Route>
          <Route path="/courses/all-goals">
            <AllGoals />
          </Route>
          <Route path="/courses/:courseId">
            <CourseGoals />
          </Route>
        </Switch>
        <Redirect path="/courses" to="/courses/list" exact />
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton href="/courses/list" tab="courses">
          <IonIcon icon={list}></IonIcon>
          <IonLabel>Courses</IonLabel>
        </IonTabButton>
        <IonTabButton href="/courses/all-goals" tab="all-goals">
          <IonIcon icon={trophyOutline}></IonIcon>
          <IonLabel>All Goals</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
};

export default CourseTabs;
